﻿using Mono.Unix;
using Mono.Unix.Native;
using Nancy.Hosting.Self;
using System;

namespace Nancy
{
	class Program
	{
		static void Main(string[] args)
		{
			var uri = "http://localhost:8888";
			using (var host = new NancyHost(new Uri("http://localhost:8888")))
			{
				Console.WriteLine("Starting Nancy on " + uri);
				host.Start();
				Console.ReadLine ();
			};
		}
	}
}