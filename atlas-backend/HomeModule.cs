﻿using System;
using Nancy;
using Nancy.ModelBinding;
using MySql.Data.MySqlClient;
using System.Data;
using System.Xml.Serialization;
using System.Windows.Forms;
using System.IO;

namespace Nancy
{
	public class city_data
	{
		public int latitude { get; set; }
		public int longitude { get; set; }
		public city_info info { get; set; }
	}

	public class city_info
	{
		public string hotel { get; set; }
		public string restaurant { get; set; }
		public string attraction { get; set; }
	}

	public class HomeModule : NancyModule
	{
		public city_data info_model = new city_data
		{
			latitude = 0,
			longitude = 0,
			info = new city_info
			{
				hotel = "",
				restaurant = "",
				attraction = ""
			}
		};

		void InsertData()
		{
			string connectionString = @"server = localhost;
										userid = emilio;
										password = wd405x2g;
										database = atlas_db";

			string query = "INSERT INTO `atlas_db`.`city_info` (`latitude`, `longitude`, `hotel`, `restaurant`, `attraction`) " +
				"VALUES ('" + info_model.latitude + "', '" + info_model.longitude + "', '" + info_model.info.hotel + "'" +
				", '" + info_model.info.restaurant + "', '" + info_model.info.attraction + "' )";

			MySqlConnection connection = new MySqlConnection(connectionString);
			connection.Open();

			MySqlCommand command = connection.CreateCommand ();
			command.CommandText = query;
			command.ExecuteNonQuery ();
			connection.Close();
		}

		public HomeModule()
		{
			Get ["/"] = _ => View["index"];

			Post["/"] = _ =>
			{
				var model = this.Bind<city_data>();
				info_model = model;
				InsertData();
				return model;
			};

			StaticConfiguration.DisableErrorTraces = false;
		}
	}
}